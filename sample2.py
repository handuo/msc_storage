from PIL import Image
import torch
from transformers import AutoModelForCausalLM, AutoProcessor
import time
import gradio as gr

model_path = "./"

kwargs = {}
kwargs['torch_dtype'] = torch.bfloat16

processor = AutoProcessor.from_pretrained(model_path, trust_remote_code=True)
model = AutoModelForCausalLM.from_pretrained(model_path, trust_remote_code=True, torch_dtype="auto").cuda()

user_prompt = '<|user|>\n'
assistant_prompt = '<|assistant|>\n'
prompt_suffix = "<|end|>\n"

def image_inference(image):
    # prompt = "\nExtract the salient elements from the image into no more than 4 keywords or tags.\n"
    prompt = f"{user_prompt}<|image_1|>\nWhat is shown in this image?{prompt_suffix}{assistant_prompt}"
    start_time = time.time()
    if isinstance(image, Image.Image):
        print(f"Image size: {image.size}, Image mode: {image.mode}")
    else:
        print("Image is not a PIL Image")

    try:
        inputs = processor(prompt, image, return_tensors="pt").to("cuda:0")
        print(f"Processor inputs: {inputs}")
    except Exception as e:
        print(f"Error during processing: {e}")
        return str(e), "N/A"
    try:
        generate_ids = model.generate(**inputs, 
                                    max_new_tokens=1000,
                                    eos_token_id=processor.tokenizer.eos_token_id,
                                    )
        generate_ids = generate_ids[:, inputs['input_ids'].shape[1]:]
        response = processor.batch_decode(generate_ids, 
                                        skip_special_tokens=True, 
                                        clean_up_tokenization_spaces=False)[0]
        elapsed_time = time.time() - start_time
        return response, f"{elapsed_time:.2f}s"
    except Exception as e:
        print(f"Error during generation: {e}")
        return str(e), "N/A"

interface = gr.Interface(
    fn=image_inference,
    inputs=gr.Image(type="pil"),
    outputs=[gr.Textbox(label="Response"), gr.Textbox(label="Time Elapsed")],
    title="Image Understanding Inference",
    description="Upload an image and click start to extract keywords/tags from the image."
)

interface.launch(share=True)
