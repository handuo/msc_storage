FROM pytorch/pytorch:2.3.1-cuda12.1-cudnn8-devel

WORKDIR /app
RUN mkdir -p /app/Phi-3-mini-128k-instruct
COPY ./models--microsoft--Phi-3-vision-128k-instruct/ /app/Phi-3-mini-128k-instruct/

RUN FLASH_ATTENTION_FORCE_BUILD=TRUE pip install flash-attn
RUN pip install vllm==0.5.0.post1 --no-cache-dir

# Set default command
CMD ["python", "-m", "vllm.entrypoints.openai.api_server", \
    "--host", "0.0.0.0", "--port", "8081", \
    "--model", "/app/Phi-3-mini-128k-instruct/snapshots/7b92b8c62807f5a98a9fa47cdfd4144f11fbd112", \
    "--served-model-name","Phi-3-mini-128k-instruct"]
