from openai import OpenAI
import base64
import time

client = OpenAI(base_url='http://0.0.0.0:23333/v1')
model_name = client.models.list().data[0].id


def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
    return encoded_string


def send_request(prompt, image_path, temperature, top_p, stream=True):
    start_time = time.time()
    base64_image = encode_image(image_path)
    encode_time = time.time() - start_time
    print(f"Image encoding time: {encode_time}")
    response = client.chat.completions.create(
        model=model_name,
        messages=[{
            'role':
            'user',
            'content': [{
                'type': 'text',
                'text': prompt,
            }, {
                'type': 'image_url',
                'image_url': {
                    'url': f"data:image/jpeg;base64,{base64_image}",
                },
            }],
        }],
        temperature=temperature,
        top_p=top_p,
        max_tokens=1024,
        stream=stream,
        # frequency_penalty=0.5,
        # presence_penalty=0.5
    )
    process_time = time.time() - start_time
    print(f"Process time: {process_time}")
    if stream is True:
        text = ''
        for info in response:
            text += info.choices[0].delta.content
            yield text
    else:
        text = response.choices[0].message.content
        return text

if __name__ == '__main__':
    # for text in send_request('Describe the image please', 'tiger.jpeg', 0.8, 0.8, stream=True):
    #     print(text)
    response = send_request('Describe the image please', 'tiger.jpeg', 0.8, 0.8, stream=False)
    print(response)
