from lmdeploy import pipeline, PytorchEngineConfig, ChatTemplateConfig
from lmdeploy.vl import load_image
import time

model = 'OpenGVLab/InternVL2-4B'
system_prompt = """You are an advanced AI assistant specialized in analyzing images and extracting meaningful 
    information. Your task is to examine images carefully, understand their overall scenario and meaning, and provide 
    high-level keywords that accurately represent the content and context of the image."""
user_prompt = """
    Please analyze the provided image and extract the most related keywords: List at most 3 key words that best 
    represent the main elements or themes or particularly striking elements in the image.
    If the image contains a celebrity or influencer, please identify them and include their name in the keywords.

The output should be in JSON format and include the following fields:
{
    "keywords": ["keyword1", "keyword2", "keyword3"]
}
"""

start_time = time.time()
chat_template_config = ChatTemplateConfig('internvl-phi3')
chat_template_config.meta_instruction = system_prompt
pipe = pipeline(model, chat_template_config=chat_template_config,
                backend_config=TurbomindEngineConfig(tp=1))
pipe_time = time.time()
image1 = load_image('https://raw.githubusercontent.com/open-mmlab/mmdeploy/main/tests/data/tiger.jpeg')
response = pipe((user_prompt, image1))
print(response.text)
print(f"pipe time: {pipe_time - start_time}")
img1_time = time.time()
print(f"img1 inference time: {img1_time - pipe_time}")
image2 = load_image('https://pbs.twimg.com/media/GSkYmxCW4AANtD3.jpg')
response2 = pipe((user_prompt, image1))
print(response2.text)
print(f"img2 inference time: {time.time() - img1_time}")