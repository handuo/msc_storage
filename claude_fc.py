# """
# Shows how to use tools with the Converse API and the claude 3.5 model.
# """
import logging
import json
import boto3

from botocore.exceptions import ClientError


class StationNotFoundError(Exception):
    """Raised when a radio station isn't found."""
    pass

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def call_tool(tool_name, tool_input):
    """Mock function to simulate calling a tool and returning a result."""
    # This should be replaced with actual API calls or function calls
    # For now, it returns a mocked response
    return {"mocked": "result"}


def generate_text(bedrock_client, model_id, tool_config, input_text):
    """Generates text using the supplied Amazon Bedrock model. If necessary,
    the function handles tool use requests and sends the result to the model.
    Args:
        bedrock_client: The Boto3 Bedrock runtime client.
        model_id (str): The Amazon Bedrock model ID.
        tool_config (dict): The tool configuration.
        input_text (str): The input text.
    Returns:
        Nothing.
    """

   # Create the initial message from the user input.
    messages = [{
        "role": "user",
        "content": [{"text": input_text}]
    }]

    response = bedrock_client.converse(
        modelId=model_id,
        messages=messages,
        toolConfig=tool_config
    )

    output_message = response['output']['message']
    messages.append(output_message)
    stop_reason = response['stopReason']

    if stop_reason == 'tool_use':
        # Tool use requested. Call the tool and send the result to the model.
        tool_requests = response['output']['message']['content']
        for tool_request in tool_requests:
            if 'toolUse' in tool_request:
                tool_name = tool_request['toolUse']['name']
                tool_input = tool_request['toolUse']['input']
                tool_use_id = tool_request['toolUse']['toolUseId']
                
                logger.info("Requesting tool %s. Request: %s",
                            tool_name, tool_use_id)

                # Call the tool (this part is mocked as we don't have the actual implementation)
                tool_result = call_tool(tool_name, tool_input)
                
                tool_result_message = {
                    "role": "assistant",
                    "content": [
                        {
                            "toolResult": {
                                "toolUseId": tool_use_id,
                                "content": tool_result,
                                "status": "success"
                            }
                        }
                    ]
                }
                
                messages.append(tool_result_message)
                
                # Send the tool result to the model.
                response = bedrock_client.converse(
                    modelId=model_id,
                    messages=messages,
                    toolConfig=tool_config
                )
                output_message = response['output']['message']

    # print the final response from the model.
    for content in output_message['content']:
        print(json.dumps(content, indent=4))


def main():
    """
    Entrypoint for tool use example of Anthropic Claude 3.5 Sonnet.
    """

    logging.basicConfig(level=logging.INFO,
                        format="%(levelname)s: %(message)s")

    model_id = "anthropic.claude-3-5-sonnet-20240620-v1:0"
    input_text = "Get the user ID and other details for the Twitter accounts with the following IDs: 123456789, 987654321, and 555555555. Include all available information for each user."

    tool_config = {
        "tools": [
            {
                "toolSpec": {
                    "name": "retrieveMultipleUsersWithIDs",
                    "description": "Returns a variety of information about one or more users specified by the requested IDs.",
                    "inputSchema": {
                        "json": {
                            "type": "object",
                            "properties": {
                                "ids": {
                                    "type": "array",
                                    "description": "A comma-separated list of user IDs. Up to 100 are allowed in a single request. Make sure to not include a space between commas and fields.",
                                    "items": {
                                        "type": "string"
                                    }
                                },
                                "expansions": {
                                    "type": "string",
                                    "description": "Expansions enable you to request additional data objects that relate to the originally returned users. The ID that represents the expanded data object will be included directly in the user data object, but the expanded object metadata will be returned within the includes response object, and will also include the ID so that you can match this data object to the original Tweet object. At this time, the only expansion available to endpoints that primarily return user objects is expansions=pinned_tweet_id. You will find the expanded Tweet data object living in the includes response object.",
                                    "enum": [
                                        "pinned_tweet_id"
                                    ]
                                },
                                "tweet_fields": {
                                    "type": "array",
                                    "description": "This fields parameter enables you to select which specific Tweet fields will deliver in each returned pinned Tweet. Specify the desired fields in a comma-separated list without spaces between commas and fields. The Tweet fields will only return if the user has a pinned Tweet and if you've also included the expansions=pinned_tweet_id query parameter in your request. While the referenced Tweet ID will be located in the original Tweet object, you will find this ID and all additional Tweet fields in the includes data object.",
                                    "items": {
                                        "type": "string",
                                        "enum": [
                                            "attachments",
                                            "author_id",
                                            "context_annotations",
                                            "conversation_id",
                                            "created_at",
                                            "edit_controls",
                                            "entities",
                                            "geo",
                                            "id",
                                            "in_reply_to_user_id",
                                            "lang",
                                            "non_public_metrics",
                                            "public_metrics",
                                            "organic_metrics",
                                            "promoted_metrics",
                                            "possibly_sensitive",
                                            "referenced_tweets",
                                            "reply_settings",
                                            "source",
                                            "text",
                                            "withheld"
                                        ]
                                    }
                                },
                                "user_fields": {
                                    "type": "array",
                                    "description": "This fields parameter enables you to select which specific user fields will deliver with each returned users objects. Specify the desired fields in a comma-separated list without spaces between commas and fields. These specified user fields will display directly in the user data objects.",
                                    "items": {
                                        "type": "string",
                                        "enum": [
                                            "created_at",
                                            "description",
                                            "entities",
                                            "id",
                                            "location",
                                            "most_recent_tweet_id",
                                            "name",
                                            "pinned_tweet_id",
                                            "profile_image_url",
                                            "protected",
                                            "public_metrics",
                                            "url",
                                            "username",
                                            "verified",
                                            "verified_type",
                                            "withheld"
                                        ]
                                    }
                                }
                            },
                            "required": [
                                "ids"
                            ]
                        }
                    }
                }
            }
        ]
    }
    bedrock_client = boto3.client(service_name='bedrock-runtime', region_name='us-east-1')

    try:
        print(f"Question: {input_text}")
        generate_text(bedrock_client, model_id, tool_config, input_text)

    except ClientError as err:
        message = err.response['Error']['Message']
        print(f"A client error occurred: {message}")

    else:
        print(
            f"Finished generating text with model {model_id}.")


if __name__ == "__main__":
    main()
